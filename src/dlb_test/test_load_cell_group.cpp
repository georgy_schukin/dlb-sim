#include <gtest/gtest.h>

#include "grid/load_cell_group.h"
#include "grid/load_cell.h"

using namespace dlbs;

class LoadCellGroupTest : public ::testing::Test {
protected:
    virtual void SetUp() {
        cells.insert(Coord(0, 0));
    }

    virtual void TearDown() {
    }

protected:
    LoadCellGroup cells;
};

TEST_F(LoadCellGroupTest, InsertTest) {
    ASSERT_EQ (cells.size(), 1);
    cells.insert(Coord(0, 1));
    ASSERT_EQ (cells.size(), 2);
    cells.insert(LoadCell::newInstance(Coord(0, 0)));
    ASSERT_EQ (cells.size(), 2);
    ASSERT_TRUE (cells.contains(Coord(0, 1)));
    ASSERT_TRUE (cells.contains(LoadCell::newInstance(Coord(0, 0))));
}

TEST_F(LoadCellGroupTest, RemoveTest) {
    cells.remove(LoadCell::newInstance(Coord(0, 0)));
    ASSERT_TRUE (cells.empty());
    cells.insert(LoadCell::newInstance(Coord(0, 1)));
    ASSERT_TRUE (cells.contains(Coord(0, 1)));
    cells.remove(Coord(0, 1));
    ASSERT_TRUE (cells.empty());
}

TEST_F(LoadCellGroupTest, IntersectionTest) {
    LoadCellGroup intersec = cells.getIntersection(LoadCellGroup({Coord(0, 0), Coord(0, 1)}));
    ASSERT_TRUE (intersec.contains(Coord(0, 0)));
    ASSERT_FALSE (intersec.contains(Coord(0, 1)));
}

TEST_F(LoadCellGroupTest, UnionTest) {
    LoadCellGroup unn = cells.getUnion(LoadCellGroup({Coord(0, 0), Coord(0, 1)}));
    ASSERT_TRUE (unn.contains(Coord(0, 0)));
    ASSERT_TRUE (unn.contains(Coord(0, 1)));
}

TEST_F(LoadCellGroupTest, ConnectivitySplitTest) {
    cells.insert(Coord(2, 2));
    cells.insert(Coord(2, 3));
    auto groups = cells.splitByConnectivity();
    ASSERT_EQ (groups.size(), 2);
}

TEST_F(LoadCellGroupTest, ConnectivitySplitTest2) {
    cells.insert(Coord(0, 1));
    cells.insert(Coord(1, 1));
    auto groups = cells.splitByConnectivity();
    ASSERT_EQ (groups.size(), 1);
}

TEST_F(LoadCellGroupTest, ConnectivityTestFalse) {
    cells.insert(Coord(2, 2));
    ASSERT_FALSE (cells.isConnected());
}

TEST_F(LoadCellGroupTest, ConnectivityTestTrue) {
    cells.insert(Coord(0, 1));
    ASSERT_TRUE (cells.isConnected());
}

TEST_F(LoadCellGroupTest, ConnectivityTestWithExcluded) {
    cells.insert(Coord(0, 1));
    cells.insert(Coord(0, 2));
    ASSERT_FALSE (cells.isConnected(LoadCellGroup({Coord(0, 1)})));
    ASSERT_TRUE (cells.isConnected(LoadCellGroup({Coord(0, 2)})));
}

