#include <gtest/gtest.h>

#include "node/node_grid.h"

#include <memory>

using namespace dlbs;

class NodeGridTest : public ::testing::Test {
protected:
    virtual void SetUp() {
        grid = std::make_unique<NodeGrid>(4, 4, 16, 16);
    }

    virtual void TearDown() {
    }

protected:
    std::unique_ptr<NodeGrid> grid;
};

TEST_F(NodeGridTest, RankToCoordTest) {
    auto c1 = grid->rankToCoord(0);
    auto c2 = grid->rankToCoord(1);
    auto c3 = grid->rankToCoord(5);
    ASSERT_EQ(c1.getDistance(c2), 1);
    ASSERT_EQ(c1.getDistance(c3), 1);
}


