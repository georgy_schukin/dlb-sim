#include <gtest/gtest.h>

#include "coord.h"

using namespace dlbs;

TEST(CoordTests, EqualTest) {
    ASSERT_TRUE (Coord(1, 2) == Coord(1, 2));
}

TEST(CoordTests, LessTest1) {
    ASSERT_TRUE (Coord(1, 2) < Coord(1, 3));
}

TEST(CoordTests, LessTest2) {
    ASSERT_TRUE (Coord(1, 2) < Coord(2, 1));
}

TEST(CoordTests, DistanceTest) {
    Coord coord(1, 1);
    ASSERT_EQ (coord.getDistance(Coord(3, 3)), 2);
    ASSERT_EQ (coord.getDistance(Coord(1, 4)), 3);
    ASSERT_EQ (coord.getDistance(Coord(3, 0)), 2);
    ASSERT_EQ (coord.getDistance(Coord(0, 4)), 3);
}

