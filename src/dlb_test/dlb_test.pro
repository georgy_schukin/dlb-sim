TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

win32 {
    INCLUDEPATH += "D:/Distribs/local/include"
    LIBS += -L"D:/Distribs/local/lib"
}

LIBS += -lgtest_main -lgtest -lpthread

SOURCES += \
    test_coord.cpp \
    test_load_cell.cpp \
    test_load_grid.cpp \
    test_load_cell_group.cpp \
    test_node_grid.cpp
    
include(../dlb_lib.pri)    




