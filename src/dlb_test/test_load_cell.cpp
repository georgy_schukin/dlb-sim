#include <gtest/gtest.h>

#include "grid/load_cell.h"

using namespace dlbs;

class LoadCellTest : public ::testing::Test {
protected:
    virtual void SetUp() {
        cell = LoadCell::newInstance(Coord(1, 1));
    }

    virtual void TearDown() {
    }

protected:
    LoadCellPtr cell;
};

TEST_F(LoadCellTest, ChangeLoadTest) {
    cell->changeLoad(1);
    ASSERT_EQ (cell->getLoad(), 1);
    cell->changeLoad(1);
    ASSERT_EQ (cell->getLoad(), 2);
    cell->changeLoad(-2);
    ASSERT_EQ (cell->getLoad(), 0);
}

TEST_F(LoadCellTest, AdjacencyTest) {
    cell->setAdjacency(NORTH, 1);
    ASSERT_TRUE (cell->hasAdjacency(NORTH));
    ASSERT_FALSE (cell->hasAdjacency(SOUTH));
    ASSERT_EQ (cell->getAdjacentRank(NORTH), 1);
    cell->setAdjacency(SOUTH, 2);
    ASSERT_TRUE (cell->isAdjacentWith(2));
}



