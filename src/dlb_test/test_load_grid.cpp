#include <gtest/gtest.h>

#include "grid/load_grid.h"
#include "grid/load_cell.h"
#include "grid/load_cell_group.h"

using namespace dlbs;

class LoadGridTest : public ::testing::Test {
protected:
    virtual void SetUp() {
        grid = std::make_shared<LoadGrid>(Region2D(0, 2, 0, 2), 1);
    }

    virtual void TearDown() {
    }

protected:
    LoadGridPtr grid;
};

TEST_F(LoadGridTest, HasCellTest) {
    ASSERT_TRUE (grid->hasCell(Coord(1, 1)));
    ASSERT_FALSE (grid->hasCell(Coord(2, 2)));
}

TEST_F(LoadGridTest, AdjacencyTest) {
    ASSERT_TRUE (grid->getCell(Coord(1, 1))->isAdjacentWith(1));
    ASSERT_FALSE (grid->getCell(Coord(0, 0))->hasAdjacency(NORTH));
}

TEST_F(LoadGridTest, AddCellsTest) {
    auto cells = {LoadCell::newInstance(Coord(1, 2)),
                  LoadCell::newInstance(Coord(2, 2))};
    grid->addCells(LoadCellGroup(cells));
    ASSERT_TRUE (grid->hasCell(Coord(2, 2)));
    ASSERT_TRUE (grid->getCell(Coord(2, 2))->isAdjacentWith(1));
    ASSERT_EQ (grid->getCell(Coord(1, 1))->getAdjacentRank(SOUTH), 1);
    ASSERT_EQ (grid->getCell(Coord(1, 2))->getAdjacentRank(NORTH), 1);
}

TEST_F(LoadGridTest, removeCellsTest) {
    auto coords = {Coord(1, 0), Coord(1, 1)};
    grid->removeCellsFor(coords, 2);
    ASSERT_FALSE (grid->hasCell(Coord(1, 0)));
    ASSERT_TRUE (grid->getCell(Coord(0, 1))->isAdjacentWith(2));
    ASSERT_EQ (grid->getCell(Coord(0, 1))->getAdjacentRank(EAST), 2);
}
