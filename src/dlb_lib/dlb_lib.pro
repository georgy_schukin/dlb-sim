TEMPLATE = lib
CONFIG += static c++14
CONFIG -= app_bundle
CONFIG -= qt

TARGET = dlbsim

LIBS += -lpthread

SOURCES += region2d.cpp \
    comm/topology.cpp \
    node/node.cpp \
    node/node_group.cpp \
    node/patch_node.cpp \
    util.cpp \
    node/node_grid.cpp \
    grid/load_cell.cpp \
    grid/load_grid.cpp \
    exec/exec_id.cpp \
    exec/task.cpp \
    types.cpp \
    grid/load_cell_group.cpp \
    sim/random_load_simulator.cpp \
    sim/wave_load_simulator.cpp \
    sim/halve_load_simulator.cpp \
    coord.cpp

HEADERS += \
    comm/topology.h \
    dlb/patch/patch_load_balancer.h \
    dlb/rope/rope_load_balancer.h \
    fragment/fragment.h \
    fragment/fragment_mesh.h \
    node/node.h \
    node/node_group.h \
    node/patch_node.h \
    node/patch_node_environment.h \
    node/thread_safe_queue.h \
    region2d.h \
    types.h \
    util.h \
    node/node_grid.h \
    grid/load_grid.h \
    grid/load_cell.h \
    grid/load_cell_ptr.h \
    exec/task.h \
    exec/data.h \
    exec/placeable.h \
    dlb/load_balancer.h \
    exec/exec_id.h \
    exec/data_ptr.h \
    grid/load_grid_ptr.h \
    grid/load_cell_group.h \
    sim/load_simulator.h \
    sim/random_load_simulator.h \
    sim/wave_load_simulator.h \
    sim/halve_load_simulator.h \
    coord.h
