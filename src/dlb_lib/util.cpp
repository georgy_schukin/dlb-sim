#include "util.h"

namespace dlbs {

std::vector<int> slice(int size, int num) {
    std::vector<int> result(num, 0);
    int div = size / num;
    int rest = size % num;
    for (int i = 0; i < num; i++) {
        result[i] = div + (i < rest ? 1 : 0);
    }
    return result;
}

std::vector<int> to_increments(const std::vector<int> &v) {
    std::vector<int> result(1, 0);
    for (int num: v) {
        result.push_back(result.back() + num);
    }
    return result;
}

}
