#include "sim/halve_load_simulator.h"
#include "node/node_grid.h"
#include "node/patch_node.h"

#include <cstdlib>

namespace dlbs {

HalveLoadSimulator::HalveLoadSimulator(double cx, double cy) :
    coeff_x(cx), coeff_y(cy) {
}

void HalveLoadSimulator::simulate(NodeGrid *grid) {
    grid->forEachNode([this, grid](PatchNode *node) {
        const CoordArray coords = node->getCurrentCoords();
        for (const Coord &coord: coords) {
            if ((coord.getX() >= grid->getIndicesRegion().getRight()*coeff_x) ||
                (coord.getY() >= grid->getIndicesRegion().getBottom()*coeff_y)) {
                continue;
            }
            LoadType load_upd = LoadType(rand() % 1000) / 1000.0;
            node->changeLoad(coord, load_upd);
        }
    });
}

}
