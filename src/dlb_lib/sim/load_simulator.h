#pragma once

namespace dlbs {

class NodeGrid;

class LoadSimulator {
public:
    virtual ~LoadSimulator() {}

    virtual void simulate(NodeGrid *grid) = 0;
};

}

