#include "sim/random_load_simulator.h"
#include "node/node_grid.h"
#include "node/patch_node.h"

#include <cstdlib>

namespace dlbs {

void RandomLoadSimulator::simulate(NodeGrid *grid) {
    grid->forEachNode([](PatchNode *node) {
        const CoordArray coords = node->getCurrentCoords();
        for (const Coord &coord: coords) {
            if (rand() % 3 == 0) {
                LoadType load_upd = LoadType(rand() % 1000) / 1000.0;
                node->changeLoad(coord, load_upd);
            }
        }
    });
}

}
