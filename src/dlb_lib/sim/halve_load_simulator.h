#pragma once

#include "sim/load_simulator.h"

namespace dlbs {

class HalveLoadSimulator : public LoadSimulator {
public:
    HalveLoadSimulator(double cx=1.0, double cy=1.0);

    virtual void simulate(NodeGrid *grid);

private:
    double coeff_x, coeff_y;
};

}

