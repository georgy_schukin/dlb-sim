#pragma once

#include "sim/load_simulator.h"

namespace dlbs {

class WaveLoadSimulator : public LoadSimulator {
public:
    WaveLoadSimulator() {}

    virtual void simulate(NodeGrid *grid);
};

}

