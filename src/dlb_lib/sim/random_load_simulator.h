#pragma once

#include "sim/load_simulator.h"

namespace dlbs {

class RandomLoadSimulator : public LoadSimulator {
public:
    RandomLoadSimulator() {}

    virtual void simulate(NodeGrid *grid);
};

}

