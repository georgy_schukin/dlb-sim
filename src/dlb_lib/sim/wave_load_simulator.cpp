#include "sim/wave_load_simulator.h"
#include "node/node_grid.h"
#include "node/patch_node.h"

#include <cmath>
#include <cstdlib>

namespace dlbs {

void WaveLoadSimulator::simulate(NodeGrid *grid) {
    const auto width = grid->getIndicesRegion().getWidth();
    const auto height = grid->getIndicesRegion().getHeight();
    const auto max_dist = sqrt(width*width + height*height);
    grid->forEachNode([max_dist, grid](PatchNode *node) {
        const CoordArray coords = node->getCurrentCoords();
        for (const Coord &coord: coords) {
            const auto coeff = (rand() % 1000) / 1000.0;
            const auto x = coord.getX();
            const auto y = coord.getY();
            LoadType load_upd = coeff*LoadType(sqrt(x*x + y*y))/max_dist;
            //LoadType load_upd = LoadType(node->getRank()*1000 + rand() % 1000)/1000.0;
            node->changeLoad(coord, load_upd);
        }
    });
}

}
