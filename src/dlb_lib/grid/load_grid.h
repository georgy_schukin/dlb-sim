#pragma once

#include "coord.h"
#include "region2d.h"
#include "grid/load_cell_ptr.h"
#include "grid/load_grid_ptr.h"
#include "grid/load_cell_group.h"
#include "types.h"

#include <map>
#include <set>
#include <memory>

namespace dlbs {

class LoadGrid {
public:
    LoadGrid(const Region2D &reg, int rank);
    LoadGrid(const LoadGrid &grid);
    ~LoadGrid() {}

    LoadGrid& operator=(const LoadGrid &grid);

    // Set adjacency for adjacenct cells to neigh rank.
    void updateAdjacency(const CoordArray &coords, int neigh_rank);
    void updateAdjacency(const Coord &coord, DirectionType dir, int neigh_rank);
    void updateAdjacency(const CoordArray &coords, DirectionType dir, int neigh_rank);

    void setUnsetAdjacency(DirectionType dir, int neigh_rank);

    CoordArray getCurrentCoords() const;
    LoadCellGroup getCurrentCells() const;

    void changeLoad(const Coord &coord, LoadType load_change);

    LoadType getCurrentLoad() const;

    bool hasCell(const Coord &coord) const;

    void addCells(const LoadCellGroup &new_cells);

    LoadCellPtr removeCell(const Coord &coord);
    LoadCellGroup removeCells(const CoordArray &coords);

    // Remove cells for transfer to dest rank.
    LoadCellGroup removeCellsFor(const CoordArray &coords, int dest_rank);
    LoadCellGroup removeCellFor(const Coord &coord, int dest_rank);

    LoadCellGroup getBorderCells(DirectionType dir) const;

    LoadCellGroup getAdjacentCells(int dest_rank) const;

    LoadCellPtr getCell(const Coord &coord) const;

    LoadCellGroup removeCellsForLoad(LoadType load, int dest_rank, DirectionType dir);

    std::set<int> getCandidateRanks(const Coord &coord) const;
    LoadCellGroup getNearestCells(const Coord &coord) const;

    size_t size() const;

    void addNeighbour(int neigh_rank);

private:
    void initCells(const Region2D &reg);
    void initCellsInnerAdjacency(const LoadCellGroup &group);
    void initCellInnerAdjacency(const LoadCellPtr &cell);

    CoordArray getCellCoordsForLoad(LoadType load, int dest_rank, DirectionType dir);

    LoadCellGroup getCellsForLoadByAdjacencyGrow(LoadType load, int dest_rank, DirectionType dir);
    LoadCellGroup getCellsForLoadByAdjacencyMinimization(LoadType load, int dest_rank, DirectionType dir);

    LoadCellGroup getStartCandidates(int dest_rank, DirectionType dir);
    std::vector<LoadCellGroup> getStartCellGroups(int dest_rank, DirectionType dir);
    LoadCellPtr getStartCell(int dest_rank, DirectionType dir);

    LoadCellGroup grow(const LoadCellGroup &start, LoadType load);
    LoadCellPtr selectNextBestCandidate(const LoadCellGroup &candidates, const LoadCellGroup &current, int dest_rank);

    void addAdjacent(LoadCellGroup &group) const;
    LoadCellGroup getAdjacent(const LoadCellGroup &group) const;

    // Add adjacent cells to the group. Connectivity of src group is preserved.
    void addAdjacentWithConnectivityPreserve(LoadCellGroup &group, LoadType max_load);

    int getAdjacencyCount(int dest_rank) const;
    int getAdjacencyCount(int dest_rank, const LoadCellGroup &removed) const;

    std::set<int> getAdjacentRanks(int dest_rank, const LoadCellGroup &removed) const;

    bool isAdjacentWithAllNeighbours(int dest_rank, const LoadCellGroup &removed) const;
    bool isConnected(const LoadCellGroup &removed) const;

private:
    int rank;
    LoadCellGroup cells;
    std::set<int> neigh_ranks;
};

typedef std::shared_ptr<class LoadGrid> LoadGridPtr;

}
