#pragma once

#include "grid/load_cell_ptr.h"
#include "coord.h"
#include "types.h"

#include <set>
#include <vector>

namespace dlbs {

struct cellCompare {
    bool operator()(const LoadCellPtr &c1, const LoadCellPtr &c2) const;
};

class LoadCellGroup {
public:
    typedef std::set<LoadCellPtr, cellCompare> CellSet;
    typedef CellSet::iterator iterator;
    typedef CellSet::const_iterator const_iterator;

public:
    LoadCellGroup() {}
    LoadCellGroup(const LoadCellGroup &group);
    LoadCellGroup(const LoadCellPtrArray &array);
    LoadCellGroup(const CoordArray &coords);

    ~LoadCellGroup() {}

    LoadCellGroup& operator=(const LoadCellGroup &group);
    LoadCellGroup& operator=(const LoadCellPtrArray &array);

    iterator begin() {
        return cells.begin();
    }

    const_iterator begin() const {
        return cells.begin();
    }

    iterator end() {
        return cells.end();
    }

    const_iterator end() const {
        return cells.end();
    }

    LoadCellPtr get(const Coord &coord) const;

    void insert(const LoadCellPtr &cell);
    void insert(const Coord &coord);

    void remove(const LoadCellPtr &cell);
    void remove(const Coord &coord);

    bool contains(const LoadCellPtr &cell) const;
    bool contains(const Coord &coord) const;

    LoadCellPtr find(const Coord &coord);

    bool empty() const;

    void clear();

    size_t size() const;

    CoordArray getCoords() const;
    LoadType getTotalLoad() const;
    LoadType getMaxLoad() const;

    std::set<int> getAdjacentRanks() const;
    std::set<int> getAdjacentRanksAtDirections(const std::vector<DirectionType> &dirs) const;
    std::vector<DirectionType> getDirections(const Coord &coord) const;
    int getAdjacencyCount(int rank) const;

    LoadCellGroup getNearestCells(const Coord &coord) const;

    LoadCellGroup getIntersection(const LoadCellGroup &group) const;
    LoadCellGroup getUnion(const LoadCellGroup &group) const;

    // Get connected components.
    std::vector<LoadCellGroup> splitByConnectivity() const;

    bool isConnectedWith(const LoadCellGroup &group) const;
    bool isConnected() const;
    bool isConnected(const LoadCellGroup &excluded) const;

private:
    CellSet cells;
};

}
