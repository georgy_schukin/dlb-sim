#pragma once

#include <memory>

namespace dlbs {

typedef std::shared_ptr<class LoadGrid> LoadGridPtr;

}
