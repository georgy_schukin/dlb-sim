#pragma once

#include <memory>
#include <vector>

namespace dlbs {

typedef std::shared_ptr<class LoadCell> LoadCellPtr;
typedef std::vector<LoadCellPtr> LoadCellPtrArray;

}
