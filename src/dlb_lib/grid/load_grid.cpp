#include "grid/load_grid.h"
#include "grid/load_cell.h"

#include <cassert>
#include <cmath>
#include <limits>

namespace dlbs {

namespace {
    bool containsAll(const std::set<int> &items, const std::set<int> &where) {
        for (const auto &item: items) {
            if (where.find(item) == where.end()) {
                return false;
            }
        }
        return true;
    }
}

LoadGrid::LoadGrid(const Region2D &reg, int rank) :
    rank(rank)
{
    initCells(reg);
}

LoadGrid::LoadGrid(const LoadGrid &grid) :
    rank(grid.rank), cells(grid.cells) {
}

LoadGrid& LoadGrid::operator =(const LoadGrid &grid) {
    rank = grid.rank;
    cells = grid.cells;
    return *this;
}

void LoadGrid::initCells(const Region2D &reg) {
    addCells(LoadCellGroup(reg.generateCoords()));
}

void LoadGrid::addCells(const LoadCellGroup &new_cells) {
    for (const LoadCellPtr &cell: new_cells) {
        cells.insert(cell);
    }
    //assert (cells.isConnected());
    initCellsInnerAdjacency(new_cells);
}

void LoadGrid::initCellsInnerAdjacency(const LoadCellGroup &group) {
    for (const LoadCellPtr &cell: group) {
        initCellInnerAdjacency(cell);
    }
}

void LoadGrid::initCellInnerAdjacency(const LoadCellPtr &cell) {
    for (DirectionType dir: getAllDirections()) {
        // Adjacent cell is ours - set adjacency for the cell.
        LoadCellPtr adj_cell = getCell(cell->getAdjacentCellCoord(dir));
        if (adj_cell) {
            cell->setAdjacency(dir, rank);
            adj_cell->setAdjacency(getOppositeDir(dir), rank);
        }
    }
}

void LoadGrid::updateAdjacency(const CoordArray &coords, int neigh_rank) {
    for (const Coord &coord: coords) {
        for (DirectionType dir: getAllDirections()) {
            LoadCellPtr adj_cell = getCell(coord.shift(dir));
            // Adjacent cell is ours - update its adjacency.
            if (adj_cell) {
                adj_cell->setAdjacency(getOppositeDir(dir), neigh_rank);
            }
        }
    }
}

void LoadGrid::updateAdjacency(const Coord &coord, DirectionType dir, int neigh_rank) {
    LoadCellPtr cell = getCell(coord);
    if (cell) {
        cell->setAdjacency(dir, neigh_rank);
    }
}

void LoadGrid::updateAdjacency(const CoordArray &coords, DirectionType dir, int neigh_rank) {
    for (const Coord &coord: coords) {
        updateAdjacency(coord, dir, neigh_rank);
    }
}

void LoadGrid::setUnsetAdjacency(DirectionType dir, int neigh_rank) {
    for (const LoadCellPtr &cell: cells) {
        if (!cell->hasAdjacency(dir)) {
            cell->setAdjacency(dir, neigh_rank);
        }
    }
}

bool LoadGrid::hasCell(const Coord &coord) const {
    return cells.contains(coord);
}

LoadCellPtr LoadGrid::getCell(const Coord &coord) const {
    return cells.get(coord);
}

LoadCellPtr LoadGrid::removeCell(const Coord &coord) {
    LoadCellPtr cell = getCell(coord);
    cells.remove(coord);
    return cell;
}

LoadCellGroup LoadGrid::removeCells(const CoordArray &coords) {
    LoadCellGroup result;
    for (const Coord &coord: coords) {
        result.insert(removeCell(coord));
    }
    return result;
}

LoadCellGroup LoadGrid::removeCellsFor(const CoordArray &coords, int dest_rank) {
    LoadCellGroup removed_cells = removeCells(coords);
    //assert (cells.isConnected());
    updateAdjacency(coords, dest_rank);
    return removed_cells;
}

LoadCellGroup LoadGrid::removeCellFor(const Coord &coord, int dest_rank) {
    return removeCellsFor({coord}, dest_rank);
}

LoadCellGroup LoadGrid::getBorderCells(DirectionType dir) const {
    LoadCellGroup result;
    for (const LoadCellPtr &cell: cells) {
        if (!hasCell(cell->getAdjacentCellCoord(dir))) {
            result.insert(cell);
        }
    }
    return result;
}

LoadCellGroup LoadGrid::getAdjacentCells(int dest_rank) const {
    LoadCellGroup result;
    for (const LoadCellPtr &cell: cells) {
        if (cell->isAdjacentWith(dest_rank)) {
            result.insert(cell);
        }
    }
    return result;
}

LoadCellGroup LoadGrid::removeCellsForLoad(LoadType load, int dest_rank, DirectionType dir) {
    CoordArray coords = getCellCoordsForLoad(load, dest_rank, dir);
    return removeCellsFor(coords, dest_rank);
}

CoordArray LoadGrid::getCellCoordsForLoad(LoadType load, int dest_rank, DirectionType dir) {
    //LoadCellGroup cells_to_give = getCellsForLoadByAdjacencyGrow(load, dest_rank, dir);
    LoadCellGroup cells_to_give = getCellsForLoadByAdjacencyMinimization(load, dest_rank, dir);
    return cells_to_give.getCoords();
}

LoadCellGroup LoadGrid::getCellsForLoadByAdjacencyMinimization(LoadType load, int dest_rank, DirectionType dir) {
    // Can't remove last cell.
    if (cells.size() <= 1) {
        return LoadCellGroup();
    }
    LoadCellPtr start_cell = getStartCell(dest_rank, dir);
    if (!start_cell) {
        return LoadCellGroup();
    }
    LoadCellGroup current_group({start_cell});
    while ((current_group.getTotalLoad() < load) && (current_group.size() < cells.size() - 1)) {
        auto next_cell = selectNextBestCandidate(getAdjacent(current_group), current_group, dest_rank);
        if (!next_cell) {
            break;
        }
        current_group.insert(next_cell);
    }
    return current_group;
}

LoadCellPtr LoadGrid::getStartCell(int dest_rank, DirectionType dir) {
    LoadCellGroup candidates = getStartCandidates(dest_rank, dir);
    return selectNextBestCandidate(candidates, LoadCellGroup(), dest_rank);
}

LoadCellGroup LoadGrid::getStartCandidates(int dest_rank, DirectionType dir) {
    LoadCellGroup candidates = getAdjacentCells(dest_rank);
    /*if (candidates.empty()) {
        candidates = getBorderCells(dir);
    }*/
    assert (!candidates.empty());
    return candidates;
}

LoadCellPtr LoadGrid::selectNextBestCandidate(const LoadCellGroup &candidates, const LoadCellGroup &current, int dest_rank) {
    if (candidates.empty()) {
        return LoadCellPtr();
    }
    LoadCellPtr best_cell;
    int min_adj_count = std::numeric_limits<int>::max();
    for (const LoadCellPtr &cell: candidates) {
        LoadCellGroup next = current;
        next.insert(cell);
        // Removing cell makes lose adjacency with come neighbours or connectivity - skip it.
        if (!isAdjacentWithAllNeighbours(dest_rank, next) || !isConnected(next)) {
            continue;
        }
        const auto adj_count = getAdjacencyCount(dest_rank, next);
        // Select cell that gives min adjacency count.
        if (adj_count < min_adj_count) {
            best_cell = cell;
            min_adj_count = adj_count;
        }
    }
    return best_cell;
}

LoadCellGroup LoadGrid::getCellsForLoadByAdjacencyGrow(LoadType load, int dest_rank, DirectionType dir) {
    LoadCellGroup best;
    auto min_size = std::numeric_limits<size_t>::max();
    auto min_load_diff = std::numeric_limits<LoadType>::max();
    for (const auto start: getStartCellGroups(dest_rank, dir)) {
        const LoadCellGroup group = grow(start, load);
        const auto size = group.size();
        const auto load_diff = fabs(load - group.getTotalLoad());
        if ((size < min_size) || (load_diff < min_load_diff)) {
            best = group;
            min_size = size;
            min_load_diff = load_diff;
        }
    }
    return best;
}

std::vector<LoadCellGroup> LoadGrid::getStartCellGroups(int dest_rank, DirectionType dir) {
    auto candidates = getStartCandidates(dest_rank, dir);
    if (candidates.empty()) {
        return {candidates};
    }
    return candidates.splitByConnectivity();
}

LoadCellGroup LoadGrid::grow(const LoadCellGroup &start, LoadType load) {
    LoadCellGroup group = start;
    auto prev_size = group.size();
    while (group.getTotalLoad() < load) {
        addAdjacentWithConnectivityPreserve(group, load);
        if (prev_size == group.size()) {
            break;
        }
        prev_size = group.size();
    }
    return group;
}

void LoadGrid::addAdjacent(LoadCellGroup &group) const {
    for (const LoadCellPtr &cell: getAdjacent(group)) {
        group.insert(cell);
    }
}

LoadCellGroup LoadGrid::getAdjacent(const LoadCellGroup &group) const {
    LoadCellGroup adjacent;
    for (const LoadCellPtr &cell: group) {
        for (DirectionType dir: cell->getAdjacentDirections()) {
            LoadCellPtr adj_cell = getCell(cell->getAdjacentCellCoord(dir));
            if (adj_cell && !group.contains(adj_cell)) {
                adjacent.insert(adj_cell);
            }
        }
    }
    return adjacent;
}

void LoadGrid::addAdjacentWithConnectivityPreserve(LoadCellGroup &group, LoadType max_load) {
    LoadType current_load = group.getTotalLoad();
    for (const LoadCellPtr &cell: group) {
        for (DirectionType dir: getAllDirections()) {
            LoadCellPtr adj_cell = getCell(cell->getAdjacentCellCoord(dir));
            if (!adj_cell || group.contains(adj_cell)) {
                continue;
            }
            group.insert(adj_cell);
            if (!cells.isConnected(group)) {
                group.remove(adj_cell);
            } else {
                current_load += adj_cell->getLoad();
            }
            if (current_load >= max_load) {
                return;
            }
        }
    }
}

CoordArray LoadGrid::getCurrentCoords() const {
    return cells.getCoords();
}

LoadCellGroup LoadGrid::getCurrentCells() const {
    return cells;
}

size_t LoadGrid::size() const {
    return cells.size();
}

void LoadGrid::changeLoad(const Coord &coord, LoadType load_change) {
    LoadCellPtr cell = getCell(coord);
    if (cell) {
        cell->changeLoad(load_change);
    }
}

LoadType LoadGrid::getCurrentLoad() const {
    return cells.getTotalLoad();
}

std::set<int> LoadGrid::getCandidateRanks(const Coord &coord) const {
    const auto nearest = getNearestCells(coord);
    assert (!nearest.empty());
    const auto directions_to_coord = nearest.getDirections(coord);
    assert (!directions_to_coord.empty());
    auto ranks = nearest.getAdjacentRanksAtDirections(directions_to_coord);
    assert (!ranks.empty());
    ranks.erase(rank);
    return ranks;
}

LoadCellGroup LoadGrid::getNearestCells(const Coord &coord) const {
    return cells.getNearestCells(coord);
}

int LoadGrid::getAdjacencyCount(int dest_rank) const {
    return cells.getAdjacencyCount(dest_rank);
}

int LoadGrid::getAdjacencyCount(int dest_rank, const LoadCellGroup &removed) const {
    int count = 0;
    for (const LoadCellPtr &cell: cells) {
        if (removed.contains(cell)) {
            continue;
        }
        for (DirectionType dir: cell->getAdjacentDirections()) {
            auto adj_cell = getCell(cell->getAdjacentCellCoord(dir));
            if ((adj_cell && removed.contains(adj_cell)) ||
                (cell->getAdjacentRank(dir) == dest_rank)) {
                count++;
            }
        }
    }
    return count;
}

std::set<int> LoadGrid::getAdjacentRanks(int dest_rank, const LoadCellGroup &removed) const {
    std::set<int> ranks;
    for (const LoadCellPtr &cell: cells) {
        for (DirectionType dir: cell->getAdjacentDirections()) {
            auto adj_cell = getCell(cell->getAdjacentCellCoord(dir));
            if (adj_cell && removed.contains(adj_cell)) {
                ranks.insert(dest_rank);
            } else {
                ranks.insert(cell->getAdjacentRank(dir));
            }
        }
    }
    return ranks;
}

void LoadGrid::addNeighbour(int neigh_rank) {
    neigh_ranks.insert(neigh_rank);
}

bool LoadGrid::isAdjacentWithAllNeighbours(int dest_rank, const LoadCellGroup &removed) const {
    const auto adj_ranks = getAdjacentRanks(dest_rank, removed);
    // Check that all neighbour ranks are still in adjacent ranks.
    return containsAll(neigh_ranks, adj_ranks);
}

bool LoadGrid::isConnected(const LoadCellGroup &removed) const {
    return cells.isConnected(removed);
}

}
