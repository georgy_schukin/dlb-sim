#include "grid/load_cell.h"

namespace dlbs {

LoadCell::LoadCell() :
    load(0) {
}

LoadCell::LoadCell(const Coord &coord) :
    coord(coord), load(0) {
}

void LoadCell::changeLoad(LoadType load_change) {
    load += load_change;
}

LoadType LoadCell::getLoad() const {
    return load;
}

const Coord& LoadCell::getCoord() const {
    return coord;
}

void LoadCell::setAdjacency(DirectionType dir, int adj_rank) {
    adjacent_ranks[dir] = adj_rank;
}

bool LoadCell::hasAdjacency(DirectionType dir) const {
    return adjacent_ranks.find(dir) != adjacent_ranks.end();
}

int LoadCell::getAdjacentRank(DirectionType dir) const {
    auto it = adjacent_ranks.find(dir);
    return (it != adjacent_ranks.end() ? it->second : -1);
}

std::set<int> LoadCell::getAdjacentRanks() const {
    std::set<int> ranks;
    for (const auto &p: adjacent_ranks) {
        ranks.insert(p.second);
    }
    return ranks;
}

std::vector<DirectionType> LoadCell::getAdjacentDirections() const {
    std::vector<DirectionType> dirs;
    for (const auto &p: adjacent_ranks) {
        dirs.push_back(p.first);
    }
    return dirs;
}

bool LoadCell::isAdjacentWith(int rank) const {
    for (const auto &p: adjacent_ranks) {
        if (p.second == rank) {
            return true;
        }
    }
    return false;
}

int LoadCell::getAdjacencyCountWith(int rank) const {
    int count = 0;
    for (const auto &p: adjacent_ranks) {
        if (p.second == rank) {
            count++;
        }
    }
    return count;
}

Coord LoadCell::getAdjacentCellCoord(DirectionType dir) const {
    return coord.shift(dir);
}

LoadCellPtr LoadCell::newInstance(const Coord &coord) {
    return std::make_shared<LoadCell>(coord);
}


}
