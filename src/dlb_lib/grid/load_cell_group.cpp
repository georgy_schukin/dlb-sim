#include "grid/load_cell_group.h"
#include "grid/load_cell.h"

#include <algorithm>
#include <limits>
#include <queue>

namespace dlbs {

namespace {
    bool uniteConnectedComponents(std::vector<LoadCellGroup> &groups) {
        for (auto it = groups.begin(); it != groups.end(); it++) {
            auto it2 = it;
            for (it2++; it2 != groups.end(); it2++) {
                if ((*it).isConnectedWith(*it2)) {
                    LoadCellGroup union_group = (*it).getUnion(*it2);
                    groups.erase(it2);
                    groups.erase(it);
                    groups.push_back(union_group);
                    return true;
                }
            }
        }
        return false;
    }

    void visit(const LoadCellPtr &start, const LoadCellGroup &group, LoadCellGroup &visited) {
        for (DirectionType dir: getAllDirections()) {
            LoadCellPtr adj_cell = group.get(start->getAdjacentCellCoord(dir));
            if (adj_cell && !visited.contains(adj_cell)) {
                visited.insert(adj_cell);
                visit(adj_cell, group, visited);
            }
        }
    }

    LoadCellGroup bfs(const LoadCellGroup &cells) {
        if (cells.empty()) {
            return LoadCellGroup();
        }
        const auto start = *(cells.begin());
        LoadCellGroup visited({start});
        std::queue<LoadCellPtr> next_cells;
        next_cells.push(start);
        while (!next_cells.empty()) {
            const auto next_cell = next_cells.front();
            next_cells.pop();
            for (DirectionType dir: next_cell->getAdjacentDirections()) {
                LoadCellPtr adj_cell = cells.get(next_cell->getAdjacentCellCoord(dir));
                if (adj_cell && !visited.contains(adj_cell)) {
                    visited.insert(adj_cell);
                    next_cells.push(adj_cell);
                }
            }
        }
        return visited;
    }
}

bool cellCompare::operator ()(const LoadCellPtr &c1, const LoadCellPtr &c2) const {
    return c1->getCoord() < c2->getCoord();
}

LoadCellGroup::LoadCellGroup(const LoadCellGroup &group) :
    cells(group.cells) {
}

LoadCellGroup::LoadCellGroup(const LoadCellPtrArray &array) {
    for (const auto &cell: array) {
        insert(cell);
    }
}

LoadCellGroup::LoadCellGroup(const CoordArray &coords) {
    for (const auto &coord: coords) {
        insert(LoadCell::newInstance(coord));
    }
}

LoadCellGroup& LoadCellGroup::operator =(const LoadCellGroup &group) {
    cells = group.cells;
    return *this;
}

LoadCellGroup& LoadCellGroup::operator =(const LoadCellPtrArray &array) {
    clear();
    for (const auto &cell: array) {
        insert(cell);
    }
    return *this;
}

LoadCellPtr LoadCellGroup::get(const Coord &coord) const {
    auto it = cells.find(LoadCell::newInstance(coord));
    return (it != cells.end() ? *it : LoadCellPtr());
}

void LoadCellGroup::insert(const LoadCellPtr &cell) {
    cells.insert(cell);
}

void LoadCellGroup::insert(const Coord &coord) {
    cells.insert(LoadCell::newInstance(coord));
}

void LoadCellGroup::remove(const LoadCellPtr &cell) {
    cells.erase(cell);
}

void LoadCellGroup::remove(const Coord &coord) {
    remove(LoadCell::newInstance(coord));
}

bool LoadCellGroup::contains(const LoadCellPtr &cell) const {
    return cells.find(cell) != cells.end();
}

bool LoadCellGroup::contains(const Coord &coord) const {
    return contains(LoadCell::newInstance(coord));
}

LoadCellPtr LoadCellGroup::find(const Coord &coord) {
    auto it = cells.find(LoadCell::newInstance(coord));
    return (it != cells.end() ? *it : LoadCellPtr());
}

bool LoadCellGroup::empty() const {
    return cells.empty();
}

void LoadCellGroup::clear() {
    cells.clear();
}

size_t LoadCellGroup::size() const {
    return cells.size();
}

CoordArray LoadCellGroup::getCoords() const {
    CoordArray coords;
    for (const LoadCellPtr &cell: cells) {
        coords.push_back(cell->getCoord());
    }
    return coords;
}

std::set<int> LoadCellGroup::getAdjacentRanks() const {
    return getAdjacentRanksAtDirections(getAllDirections());
}

std::set<int> LoadCellGroup::getAdjacentRanksAtDirections(const std::vector<DirectionType> &dirs) const {
    std::set<int> result;
    for (const LoadCellPtr &cell: cells) {
        for (DirectionType dir: dirs) {
            if (cell->hasAdjacency(dir)) {
                result.insert(cell->getAdjacentRank(dir));
            }
        }
    }
    return result;
}

std::vector<DirectionType> LoadCellGroup::getDirections(const Coord &coord) const {
    std::set<DirectionType> dirs;
    for (const LoadCellPtr &cell: cells) {
        const auto cell_dirs = cell->getCoord().getDirections(coord);
        dirs.insert(cell_dirs.begin(), cell_dirs.end());
    }
    return std::vector<DirectionType>(dirs.begin(), dirs.end());
}

int LoadCellGroup::getAdjacencyCount(int rank) const {
    int count = 0;
    for (const LoadCellPtr &cell: cells) {
        count += cell->getAdjacencyCountWith(rank);
    }
    return count;
}

LoadType LoadCellGroup::getTotalLoad() const {
    LoadType load = 0;
    for (const LoadCellPtr &cell: cells) {
        load += cell->getLoad();
    }
    return load;
}

LoadType LoadCellGroup::getMaxLoad() const {
    static auto cmp_func = [](const LoadCellPtr &c1, const LoadCellPtr &c2) {
        return c1->getLoad() < c2->getLoad();
    };

    auto it = std::max_element(cells.begin(), cells.end(), cmp_func);
    return (it != cells.end() ? (*it)->getLoad() : 0);
}

LoadCellGroup LoadCellGroup::getNearestCells(const Coord &coord) const {
    LoadCellGroup nearest;
    int min_dist = std::numeric_limits<int>::max();
    for (const LoadCellPtr &cell: cells) {
        const int dist = cell->getCoord().getDistance(coord);
        if (dist < min_dist) {
            nearest.clear();
            nearest.insert(cell);
            min_dist = dist;
        } else if (dist == min_dist) {
            nearest.insert(cell);
        }
    }
    return nearest;
}

LoadCellGroup LoadCellGroup::getIntersection(const LoadCellGroup &group) const {
    LoadCellGroup result;
    for (const LoadCellPtr &cell: group) {
        if (contains(cell)) {
            result.insert(cell);
        }
    }
    return result;
}

LoadCellGroup LoadCellGroup::getUnion(const LoadCellGroup &group) const {
    LoadCellGroup result = *this;
    for (const LoadCellPtr &cell: group) {
        result.insert(cell);
    }
    return result;
}

std::vector<LoadCellGroup> LoadCellGroup::splitByConnectivity() const {
    std::vector<LoadCellGroup> groups;
    for (const LoadCellPtr &cell: cells) {
        groups.push_back(LoadCellGroup({cell}));
    }
    while (uniteConnectedComponents(groups));
    return groups;
}

bool LoadCellGroup::isConnectedWith(const LoadCellGroup &group) const {
    for (const LoadCellPtr &cell: cells) {
        if (group.contains(cell)) {
            return true;
        }
        for (DirectionType dir: getAllDirections()) {
            if (group.contains(cell->getAdjacentCellCoord(dir))) {
                return true;
            }
        }
    }
    return false;
}

bool LoadCellGroup::isConnected() const {
    if (empty()) {
        return false;
    }
    return (bfs(*this).size() == cells.size());
}

bool LoadCellGroup::isConnected(const LoadCellGroup &excluded) const {
    LoadCellGroup cells_without_excluded = *this;
    for (const auto &cell: excluded) {
        cells_without_excluded.remove(cell);
    }
    return cells_without_excluded.isConnected();
}

}
