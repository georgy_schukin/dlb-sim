#pragma once

#include "coord.h"
#include "types.h"
#include "grid/load_cell_ptr.h"

#include <map>
#include <set>

namespace dlbs {

class LoadCell {
public:
public:
    LoadCell();
    LoadCell(const Coord &coord);
    ~LoadCell() {}

    void changeLoad(LoadType load_change);

    LoadType getLoad() const;
    const Coord& getCoord() const;

    void setAdjacency(DirectionType dir, int adj_rank);
    bool hasAdjacency(DirectionType dir) const;

    bool isAdjacentWith(int rank) const;

    int getAdjacencyCountWith(int rank) const;

    int getAdjacentRank(DirectionType dir) const;
    std::set<int> getAdjacentRanks() const;

    std::vector<DirectionType> getAdjacentDirections() const;

    Coord getAdjacentCellCoord(DirectionType dir) const;

    static LoadCellPtr newInstance(const Coord &coord);

private:
    Coord coord;
    LoadType load;
    std::map<DirectionType, int> adjacent_ranks;
};

}
