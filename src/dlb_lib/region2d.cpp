#include "region2d.h"

#include <algorithm>

namespace dlbs {

Region2D::Region2D() :
    left(0), right(0), top(0), bottom(0) {
}

Region2D::Region2D(int left, int right, int top, int bottom) :
    left(left), right(right), top(top), bottom(bottom) {
}

bool Region2D::isIn(const Coord &coord) const {
    return (left <= coord.getX()) && (coord.getX() < right) &&
            (top <= coord.getY()) && (coord.getY() < bottom);
}

bool Region2D::isSingleBy(DimensionType dim) const {
    switch (dim) {
    case DIM_X:
        return (right - left == 1);
    case DIM_Y:
        return (bottom - top == 1);
    default:
        return false;
    }
}

bool Region2D::isSingleIndex() const {
    return isSingleBy(DIM_X) && isSingleBy(DIM_Y);
}

bool Region2D::isEmpty() const {
    return (left == right) || (top == bottom);
}

Coord Region2D::getTopLeft() const {
    return Coord(left, top);
}

int Region2D::getWidth() const {
    return right - left;
}

int Region2D::getHeight() const {
    return bottom - top;
}

CoordArray Region2D::generateCoords() const {
    CoordArray result;
    for (int i = top; i < bottom; i++) {
        for (int j = left; j < right; j++) {
            result.push_back(Coord(j, i));
        }
    }
    return result;
}

Region2D Region2D::getSubregion(QuadrantType quadrant) {
    const int middle_x = left + (right - left) / 2;
    const int middle_y = top + (bottom - top) / 2;
    switch (quadrant) {
    case NORTH_WEST:
        return Region2D(left, middle_x, top, middle_y);
    case NORTH_EAST:
        return Region2D(middle_x, right, top, middle_y);
    case SOUTH_WEST:
        return Region2D(left, middle_x, middle_y, bottom);
    case SOUTH_EAST:
        return Region2D(middle_x, right, middle_y, bottom);
    default:
        return Region2D();
    }
}

void Region2D::expandToInclude(const Coord &coord) {
    left = std::min(left, coord.getX());
    right = std::max(right, coord.getX());
    top = std::min(top, coord.getY());
    bottom = std::max(bottom, coord.getY());
}

}
