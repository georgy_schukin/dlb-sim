#pragma once

#include "types.h"
#include "coord.h"

namespace dlbs {

class Region2D {
public:
    Region2D();
    Region2D(int left, int right, int top, int bottom);

    bool isIn(const Coord &coord) const;

    bool isSingleBy(DimensionType dim) const;
    bool isSingleIndex() const;
    bool isEmpty() const;

    Coord getTopLeft() const;
    CoordArray generateCoords() const;

    Region2D getSubregion(QuadrantType quadrant);

    int getLeft() const {
        return left;
    }

    int getRight() const {
        return right;
    }

    int getTop() const {
        return top;
    }

    int getBottom() const {
        return bottom;
    }

    void expandToInclude(const Coord &coord);

    int getWidth() const;
    int getHeight() const;

private:
    int left, right;
    int top, bottom;        
};

}
