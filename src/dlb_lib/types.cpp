#include "types.h"

namespace dlbs {

DirectionType getOppositeDir(DirectionType dir) {
    switch (dir) {
    case NORTH:
        return SOUTH;
    case SOUTH:
        return NORTH;
    case WEST:
        return EAST;
    case EAST:
        return WEST;
    default:
        return dir;
    }    
}

std::vector<DirectionType> getAllDirections() {
    return {NORTH, SOUTH, WEST, EAST};
}

}
