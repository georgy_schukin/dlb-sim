#include "coord.h"

#include <algorithm>

namespace dlbs {

Coord::Coord() :
    x(0), y(0) {
}

Coord::Coord(int x, int y) :
    x(x), y(y) {
}

bool Coord::operator<(const Coord &coord) const {
    return (x < coord.x) ||
            ((x <= coord.x) && (y < coord.y));
}

bool Coord::operator==(const Coord &coord) const {
    return ((x == coord.x) && (y == coord.y));
}

Coord Coord::shift(DirectionType dir) const {
    switch (dir) {
    case NORTH:
        return Coord(x, y - 1);
    case SOUTH:
        return Coord(x, y + 1);
    case WEST:
        return Coord(x - 1, y);
    case EAST:
        return Coord(x + 1, y);
    default:
        return Coord(x, y);
    }    
}

int Coord::getDistance(const Coord &coord) const {
    const int dx = std::abs(coord.getX() - x);
    const int dy = std::abs(coord.getY() - y);
    return dx + dy;
}

int Coord::getHops(const Coord &coord) const {
    const int dx = std::abs(coord.getX() - x);
    const int dy = std::abs(coord.getY() - y);
    return dx + dy;
}

std::vector<DirectionType> Coord::getDirections(const Coord &coord) const {
    std::vector<DirectionType> dirs;
    if (x < coord.getX()) {
        dirs.push_back(EAST);
    } else if (x > coord.getX()) {
        dirs.push_back(WEST);
    }
    if (y < coord.getY()) {
        dirs.push_back(SOUTH);
    } else if (y > coord.getY()) {
        dirs.push_back(NORTH);
    }
    return dirs;
}

}
