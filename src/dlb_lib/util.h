#pragma once

#include <vector>

namespace dlbs {

std::vector<int> slice(int size, int num);
std::vector<int> to_increments(const std::vector<int> &v);

}
