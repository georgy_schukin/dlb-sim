#pragma once

#include "placeable.h"
#include "coord.h"
#include "exec/data_ptr.h"

#include <memory>
#include <map>

namespace dlbs {

class Task : public Placeable {
public:
    Task(const ExecId &id, const ExecIdArray &args={});

    bool isReady() const;

    void setArg(const ExecId &id, const DataPtr &data);

private:
    std::map<ExecId, DataPtr> supplied_args;
    ExecIdArray args;
};

typedef std::shared_ptr<Task> TaskPtr;

}
