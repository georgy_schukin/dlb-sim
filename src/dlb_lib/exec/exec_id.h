#pragma once

#include "coord.h"

#include <vector>
#include <string>

namespace dlbs {

class ExecId {
public:
    ExecId();
    ExecId(const std::string &name, const Coord &coord);
    ExecId(const std::string &name, int x, int y);

    const Coord& getCoord() const {
        return coord;
    }

    bool operator<(const ExecId &id) const;
    bool operator==(const ExecId &id) const;

private:
    std::string name;
    Coord coord;
};

typedef std::vector<ExecId> ExecIdArray;

}
