#pragma once

#include "placeable.h"
#include "exec/data_ptr.h"

#include <memory>

namespace dlbs {

class Data : public Placeable {
public:
    Data(const ExecId &id, int size) :
        Placeable(id),
        size(size) {
    }

    int getSize() const {
        return size;
    }

private:
    int size;
};

}
