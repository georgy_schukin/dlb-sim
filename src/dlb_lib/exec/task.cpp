#include "exec/task.h"

namespace dlbs {

Task::Task(const ExecId &id, const ExecIdArray &args) :
    Placeable(id),
    args(args) {
}

bool Task::isReady() const {
    return args.size() == supplied_args.size();
}

void Task::setArg(const ExecId &id, const DataPtr &data) {
    supplied_args[id] = data;
}

}
