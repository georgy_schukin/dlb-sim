#pragma once

#include "coord.h"
#include "exec/exec_id.h"

namespace dlbs {

class Placeable {
public:
    Placeable(const ExecId &id) :
        id(id) {
    }

    virtual ~Placeable() {}

    const ExecId& getId() const {
        return id;
    }

private:
    ExecId id;
};

}
