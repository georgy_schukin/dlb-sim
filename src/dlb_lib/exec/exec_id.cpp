#include "exec/exec_id.h"

namespace dlbs {

ExecId::ExecId() {
}

ExecId::ExecId(const std::string &name, const Coord &coord) :
    name(name), coord(coord) {
}

ExecId::ExecId(const std::string &name, int x, int y) :
    name(name), coord(x, y) {
}

bool ExecId::operator<(const ExecId &id) const {
    return (name < id.name) ||
            (name == id.name && coord < id.coord);
}

bool ExecId::operator==(const ExecId &id) const {
    return (name == id.name && coord == id.coord);
}

}
