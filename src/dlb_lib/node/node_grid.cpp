#include "node/node_grid.h"
#include "node/patch_node.h"
#include "region2d.h"
#include "patch_node.h"
#include "util.h"

#include <iostream>
#include <cmath>

namespace dlbs {

NodeGrid::NodeGrid(int width, int height, int num_of_indices_by_x, int num_of_indices_by_y) :
    width(width), height(height),
    indices_region(0, num_of_indices_by_x, 0, num_of_indices_by_y)
{
    initNodes(num_of_indices_by_x, num_of_indices_by_y);
    initNeighbours();
}

void NodeGrid::initNodes(int num_of_indices_by_x, int num_of_indices_by_y) {
    const auto borders_by_x = to_increments(slice(num_of_indices_by_x, width));
    const auto borders_by_y = to_increments(slice(num_of_indices_by_y, height));
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            const Region2D reg(borders_by_x[j], borders_by_x[j + 1], borders_by_y[i], borders_by_y[i + 1]);
            const Coord coord(j, i);
            const int rank = coordToRank(coord);
            nodes[coord] = std::make_shared<PatchNode>(rank, coord, reg, this);
        }
    }
}

void NodeGrid::initNeighbours() {
    for (const auto &p: nodes) {
        PatchNodePtr node = p.second;
        const Coord &coord = node->getCoord();
        if (coord.getY() > 0) {
            node->setNeighbour(NORTH, getNode(coordToRank(coord.getX(), coord.getY() - 1)));
        }
        if (coord.getY() < height - 1) {
            node->setNeighbour(SOUTH, getNode(coordToRank(coord.getX(), coord.getY() + 1)));
        }
        if (coord.getX() > 0) {
            node->setNeighbour(WEST, getNode(coordToRank(coord.getX() - 1, coord.getY())));
        }
        if (coord.getX() < width - 1) {
            node->setNeighbour(EAST, getNode(coordToRank(coord.getX() + 1, coord.getY())));
        }
    }
}

void NodeGrid::printInfo() {
    for (const auto &p: nodes) {
        PatchNodePtr node = p.second;
        CoordArray coords = node->getCurrentCoords();
        for (const Coord &coord: coords) {
            std::cout << "Rank " << node->getRank() << " has (" << coord.getX() << ", " << coord.getY() << ")" << std::endl;
        }
    }
}

int NodeGrid::coordToRank(const Coord &coord) const {
    return coordToRank(coord.getX(), coord.getY());
}

int NodeGrid::coordToRank(int x, int y) const {
    return y*width + x;
}

Coord NodeGrid::rankToCoord(int rank) const {
    int y = int(rank / width);
    int x = rank - y*width;
    return Coord(x, y);
}

PatchNode* NodeGrid::getNodeByCoord(const Coord &coord) {
    auto it = nodes.find(coord);
    return (it != nodes.end() ? it->second.get() : nullptr);
}

PatchNode* NodeGrid::getNode(int rank) {
    for (const auto &p: nodes) {
        if (p.second->getRank() == rank) {
            return p.second.get();
        }
    }
    return nullptr;
}

int NodeGrid::getNumOfNodes() const {
    return nodes.size();
}

void NodeGrid::forEachNode(std::function<void(PatchNode*)> func) {
    for (const auto &p: nodes) {
        func(p.second.get());
    }
}

}
