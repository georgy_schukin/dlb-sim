#pragma once

namespace dlbs {

class PatchNode;

class PatchNodeEnvironment {
public:
    virtual ~PatchNodeEnvironment() {}

    virtual PatchNode* getNode(int rank) = 0;
};

}
