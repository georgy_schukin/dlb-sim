#pragma once

#include "coord.h"
#include "region2d.h"
#include "node/patch_node_environment.h"

#include <map>
#include <memory>
#include <functional>

namespace dlbs {

class PatchNode;
typedef std::shared_ptr<PatchNode> PatchNodePtr;

class NodeGrid : public PatchNodeEnvironment {
public:
    NodeGrid(int width, int height, int num_of_indices_by_x, int num_of_indices_by_y);

    void printInfo();

    int coordToRank(const Coord &coord) const;
    int coordToRank(int x, int y) const;

    Coord rankToCoord(int rank) const;

    virtual PatchNode* getNode(int rank);
    int getNumOfNodes() const;

    PatchNode* getNodeByCoord(const Coord &coord);

    int getWidth() const {
        return width;
    }

    int getHeight() const {
        return height;
    }

    const Region2D& getIndicesRegion() const {
        return indices_region;
    }

    void forEachNode(std::function<void(PatchNode*)> func);

private:
    void initNodes(int num_of_indices_by_x, int num_of_indices_by_y);
    void initNeighbours();

private:
    std::map<Coord, PatchNodePtr> nodes;
    int width, height;
    Region2D indices_region;
};

}
