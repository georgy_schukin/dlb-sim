#pragma once

#include "node.h"

#include <map>
#include <cstddef>

namespace dlbs {

class NodeGroup {
public:
    NodeGroup() {}
    NodeGroup(size_t num_of_nodes);

    size_t numOfNodes() const {
        return nodes.size();
    }

    Node* node(size_t id);
    const Node* node(size_t id) const;

    void startAll();
    void stopAll();

    template <typename Func>
    void forEach(Func func) {
        for (auto &p: nodes) {
            func(&(p.second));
        }
    }

private:
    std::map<size_t, Node> nodes;
};

}
