#pragma once

#include <mutex>
#include <condition_variable>
#include <queue>

namespace dlbs {

template <typename T>
class ThreadSafeQueue {
public:
    ThreadSafeQueue() {}

    void push(const T &elem) {
        std::lock_guard<std::mutex> lock(mutex);
        _queue.push(elem);
        cond.notify_one();
    }

    T pop() {
        std::unique_lock<std::mutex> lock(mutex);
        if (_queue.empty()) {
            cond.wait(lock, [this]() { return !_queue.empty(); });
        }
        T elem = _queue.front();
        _queue.pop();
        return elem;
    }

private:
    std::queue<T> _queue;
    mutable std::mutex mutex;
    mutable std::condition_variable cond;
};

}
