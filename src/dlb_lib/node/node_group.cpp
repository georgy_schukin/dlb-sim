#include "node_group.h"

namespace dlbs {

Node* NodeGroup::node(size_t id) {
    auto it = nodes.find(id);
    return (it != nodes.end() ? &(it->second) : nullptr);
}

const Node* NodeGroup::node(size_t id) const {
    auto it = nodes.find(id);
    return (it != nodes.end() ? &(it->second) : nullptr);
}

void NodeGroup::startAll() {
    forEach([](Node *node) {
       node->start();
    });
}

void NodeGroup::stopAll() {
    forEach([](Node *node) {
       node->stop();
    });
}

}
