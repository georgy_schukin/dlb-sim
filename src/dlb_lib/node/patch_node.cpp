#include "node/patch_node.h"
#include "node/patch_node_environment.h"
#include "grid/load_grid.h"
#include "grid/load_cell.h"

#include <algorithm>
#include <future>
#include <iostream>

namespace dlbs {

PatchNode::PatchNode(int rank, const Coord &coord, const Region2D &region, PatchNodeEnvironment *env) :
    rank(rank), coord(coord), env(env)
{
    load_grid = std::make_shared<LoadGrid>(region, rank);
}

void PatchNode::setNeighbour(DirectionType dir, PatchNode *node) {
    std::unique_lock<std::mutex> lock(load_mutex);
    neighbours[dir] = node;
    load_grid->addNeighbour(node->getRank());
    load_grid->updateAdjacency(node->getCurrentCoords(), node->getRank());
}

CoordArray PatchNode::getCurrentCoords() const {
    std::unique_lock<std::mutex> lock(load_mutex);
    return load_grid->getCurrentCoords();
}

LoadCellGroup PatchNode::getCurrentCells() const {
    std::unique_lock<std::mutex> lock(load_mutex);
    return load_grid->getCurrentCells();
}

DirectionType PatchNode::getNeighDirection(int neigh_rank) const {
    for (const auto &p: neighbours) {
        if (p.second->getRank() == neigh_rank) {
            return p.first;
        }
    }
    return UNKNOWN_DIR;
}

void PatchNode::changeLoad(const Coord &coord, LoadType load_change) {
    std::unique_lock<std::mutex> lock(load_mutex);
    load_grid->changeLoad(coord, load_change);
}

LoadType PatchNode::getCurrentLoad() const {
    std::unique_lock<std::mutex> lock(load_mutex);
    return load_grid->getCurrentLoad();
}

LoadType PatchNode::getCurrentLoadAt(const Coord &coord) const {
    std::unique_lock<std::mutex> lock(load_mutex);
    LoadCellPtr cell = load_grid->getCell(coord);
    return (cell ? cell->getLoad() : 0);
}

void PatchNode::addCells(const LoadCellGroup &cells) {
    std::cout << "Node " << getRank() << " received " << cells.size() << " cells, load: " << cells.getTotalLoad() << std::endl;
    std::unique_lock<std::mutex> lock(load_mutex);
    load_grid->addCells(cells);
}

LoadCellGroup PatchNode::removeCellsForLoad(LoadType load, int dest_rank) {
    std::unique_lock<std::mutex> lock(load_mutex);
    return load_grid->removeCellsForLoad(load, dest_rank, getNeighDirection(dest_rank));
}

void PatchNode::updateAdjacency(const CoordArray &coords, DirectionType dir, int new_rank) {
    std::map<int, CoordArray> resends;
    {
        std::unique_lock<std::mutex> lock(load_mutex);
        for (const Coord &coord: coords) {
            if (load_grid->hasCell(coord)) {
                load_grid->updateAdjacency(coord, dir, new_rank);
            } else {
                for (auto neigh_rank: load_grid->getCandidateRanks(coord)) {
                    resends[neigh_rank].push_back(coord);
                }
            }
        }
    }
    for (const auto &p: resends) {
        env->getNode(p.first)->sendAdjacencyUpdate(p.second, dir, new_rank);
    }
}

void PatchNode::rebalance() {
    std::vector<std::pair<PatchNode*, LoadType>> neigh_load;
    for (const auto &p: neighbours) {
        PatchNode *neigh = p.second;
        neigh_load.push_back(std::make_pair(neigh, neigh->getCurrentLoad()));
    }
    std::sort(neigh_load.begin(), neigh_load.end(), [](const auto &a, const auto &b) {return a.second < b.second;});
    LoadType my_load = getCurrentLoad();
    LoadType avg_load = 0;
    for (const auto &p: neigh_load) {
        avg_load += p.second;
    }
    avg_load = (my_load + avg_load) / (neighbours.size() + 1);
    std::cout << "Node " << getRank() << " load " << my_load
              << ", avg load " << avg_load << std::endl;
    if (my_load > avg_load) {
        LoadType load_to_give = my_load - avg_load;
        for (const auto &p: neigh_load) {
            if (load_to_give <= 0) {
                break;
            }
            const LoadType n_load = p.second;
            if (n_load < avg_load) {
                const LoadType load_to_give_to_neigh = std::min(load_to_give, avg_load - n_load);
                startRebalanceWith(p.first, load_to_give_to_neigh);
                load_to_give -= load_to_give_to_neigh;
            }
        }
    }
}

void PatchNode::startRebalanceWith(PatchNode *neigh, LoadType load) {
    std::cout << "Node " << getRank() << " start rebalance with " << neigh->getRank()
              << " (load: " << load << ")" << std::endl;
    {
        std::unique_lock<std::mutex> lock(rebalance_mutex);
        giving_to.insert(neigh->getRank());
    }
    neigh->sendRebalanceRequest(this, load);
}

void PatchNode::finishRebalanceWith(PatchNode *neigh) {
    std::cout << "Node " << getRank() << " finish rebalance with " << neigh->getRank() << std::endl;
    std::unique_lock<std::mutex> lock(rebalance_mutex);
    giving_to.erase(neigh->getRank());
    receiving_from.erase(neigh->getRank());
}

void PatchNode::sendRebalanceRequest(PatchNode *src, LoadType load) {
    std::cout << "Node " << src->getRank() << " sends rebalance request to " << getRank() << " (load: " << load << ")" << std::endl;
    std::async(std::launch::async, [this, src, load] {
        bool is_accepted = false;
        {
            const int src_rank = src->getRank();
            std::unique_lock<std::mutex> lock(rebalance_mutex);
            // Cannot receive load when already giving load.
            if (!giving_to.empty() ||
                (receiving_from.find(src_rank) != receiving_from.end())) {
                is_accepted = false;
            } else {
                receiving_from.insert(src_rank);
                is_accepted = true;
            }
        }
        src->sendRebalanceResponse(this, load, is_accepted);
    });
}

void PatchNode::sendRebalanceResponse(PatchNode *src, LoadType load, bool is_accepted) {
    std::cout << "Node " << src->getRank() << " send rebalance response to " << getRank()
              << " (load: " << load << ", accepted: " << is_accepted << ")" << std::endl;
    std::async(std::launch::async, [this, src, load, is_accepted] {
        if (is_accepted) {
            rebalanceWithNeighbour(src, load);
        }
        finishRebalanceWith(src);
    });
}

void PatchNode::sendCells(PatchNode *src, const LoadCellGroup &cells) {
    if (!cells.empty()) {
        std::cout << "Node " << src->getRank() << " sends cells to " << getRank()
                  << " (count: " << cells.size() << ", load: " << cells.getTotalLoad() << ")" << std::endl;
        std::async(std::launch::async, [this, src, cells] {
            addCells(cells);
            finishRebalanceWith(src);
        });
    }
}

void PatchNode::sendAdjacencyUpdate(const CoordArray &coords, DirectionType dir, int new_rank) {
    if (!coords.empty()) {
        std::async(std::launch::async, [this, coords, dir, new_rank] {
            updateAdjacency(coords, dir, new_rank);
        });
    }
}

void PatchNode::rebalanceWithNeighbour(PatchNode *neigh, LoadType load) {
    LoadCellGroup cells_to_send = removeCellsForLoad(load, neigh->getRank());
    if (!cells_to_send.empty()) {
        updateAdjacencyForNeighbours(neigh, cells_to_send);
        neigh->sendCells(this, cells_to_send);
    }
}

void PatchNode::updateAdjacencyForNeighbours(PatchNode *neigh, const LoadCellGroup &cells) {
    std::map<int, std::map<DirectionType, CoordArray>> adjacent;
    for (const LoadCellPtr &cell: cells) {
        for (DirectionType dir: getAllDirections()) {
            if (cell->hasAdjacency(dir)) {
                int adj_rank = cell->getAdjacentRank(dir);
                if (adj_rank != rank && adj_rank != neigh->getRank()) {
                    adjacent[adj_rank][getOppositeDir(dir)].push_back(cell->getAdjacentCellCoord(dir));
                }
            }
        }
    }
    for (const auto &p: adjacent) {
        for (const auto &p2: p.second) {
            env->getNode(p.first)->sendAdjacencyUpdate(p2.second, p2.first, neigh->getRank());
        }
    }
}

}
