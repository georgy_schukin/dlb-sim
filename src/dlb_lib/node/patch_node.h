#pragma once

#include "coord.h"
#include "region2d.h"
#include "grid/load_grid_ptr.h"
#include "grid/load_cell_ptr.h"
#include "grid/load_cell_group.h"

#include <vector>
#include <memory>
#include <map>
#include <set>
#include <mutex>

namespace dlbs {

class PatchNodeEnvironment;

class PatchNode {
public:
    PatchNode(int rank, const Coord &coord, const Region2D &region, PatchNodeEnvironment *env);
    ~PatchNode() {}

    void setNeighbour(DirectionType dir, PatchNode *node);

    CoordArray getCurrentCoords() const;
    LoadCellGroup getCurrentCells() const;

    int getRank() const {
        return rank;
    }

    const Coord& getCoord() const {
        return coord;
    }

    void changeLoad(const Coord &coord, LoadType load_change);

    LoadType getCurrentLoad() const;
    LoadType getCurrentLoadAt(const Coord &coord) const;

    void addCells(const LoadCellGroup &cells);

    void rebalance();

    void startRebalanceWith(PatchNode *neigh, LoadType load);
    void finishRebalanceWith(PatchNode *neigh);

    void sendRebalanceRequest(PatchNode *src, LoadType load);
    void sendRebalanceResponse(PatchNode *src, LoadType load, bool is_accepted);
    void sendCells(PatchNode *src, const LoadCellGroup &cells);
    void sendAdjacencyUpdate(const CoordArray &coords, DirectionType dir, int new_rank);

private:
    void updateAdjacency(const CoordArray &coords, DirectionType dir, int new_rank);
    LoadCellGroup removeCellsForLoad(LoadType load, int dest_rank);

    DirectionType getNeighDirection(int neigh_rank) const;

    void rebalanceWithNeighbour(PatchNode *neigh, LoadType load);
    void updateAdjacencyForNeighbours(PatchNode *neigh, const LoadCellGroup &cells);

private:
    int rank;
    Coord coord;
    std::map<DirectionType, PatchNode*> neighbours;
    std::set<int> giving_to, receiving_from;
    LoadGridPtr load_grid;
    PatchNodeEnvironment *env;
    mutable std::mutex load_mutex;
    mutable std::mutex rebalance_mutex;
};

}
