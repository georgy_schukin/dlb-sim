#pragma once

#include "thread_safe_queue.h"

#include <thread>
#include <cstddef>
#include <future>

namespace dlbs {

class NodeGroup;

class Node {
public:
    Node() {}
    Node(size_t id) :
        _id(id) {
    }

    size_t id() const {
        return _id;
    }

    template <typename Result, typename Method, typename... Args>
    Result remoteCall(const Method &m, const Args&... args) {
        auto future = std::async(std::launch::async, m, this, args...);
        return future.get();
    }

    /*template <typename Method, typename... Args>
    void send(const Method &m, const Args&... args) {
        recv_queue.push([this, m, args...]() {
            this->*m(args...);
        });
    }*/

    void start();
    void stop();

private:
    size_t _id {0};
    std::thread recv_thread;
    std::thread exec_thread;
    using RecvTask = std::function<void()>;
    ThreadSafeQueue<RecvTask> recv_queue;
};

}
