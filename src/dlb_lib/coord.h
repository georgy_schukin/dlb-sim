#pragma once

#include "types.h"

#include <vector>

namespace dlbs {

class Coord {
public:
    Coord();
    Coord(int x, int y);

    int getX() const {
        return x;
    }

    int getY() const {
        return y;
    }

    bool operator<(const Coord &coord) const;
    bool operator==(const Coord &coord) const;

    Coord shift(DirectionType dir) const;

    // Get min distance in hops (by horiz, vert or diag).
    int getDistance(const Coord &coord) const;

    int getHops(const Coord &coord) const;

    // Get all posible directions from this coord to the given coord.
    std::vector<DirectionType> getDirections(const Coord &coord) const;

private:
    int x, y;
};

typedef std::vector<Coord> CoordArray;

}
