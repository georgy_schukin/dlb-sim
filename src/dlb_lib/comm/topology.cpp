#include "topology.h"

namespace dlbs {

void Topology::addNode(int rank) {
    if (links.find(rank) == links.end()) {
        links[rank] = std::set<int>();
    }
}

void Topology::addLink(int source, int dest) {
    links[source].insert(dest);
    links[dest].insert(source);
}

size_t Topology::getNumOfNodes() const {
    return links.size();
}

std::set<int> Topology::getAdjacentNodes(int node) const {
    auto it = links.find(node);
    return (it != links.end() ? it->second : std::set<int>());
}

Topology TopologyUtil::makeLinearTopology(size_t num_of_nodes) {
    Topology result;
    for (size_t i = 0; i < num_of_nodes - 1; i++) {
        result.addLink(i, i + 1);
    }
    return result;
}

Topology TopologyUtil::makeCart2DTopology(size_t height, size_t width) {
    Topology result;
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width - 1; j++) {
            result.addLink(i*width + j, i*width + j + 1);
        }
    }
    for (size_t i = 0; i < height - 1; i++) {
        for (size_t j = 0; i < width; j++) {
            result.addLink(i*width + j, (i + 1)*width + j);
        }
    }
    return result;
}

}
