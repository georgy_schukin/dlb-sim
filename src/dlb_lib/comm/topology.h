#pragma once

#include <map>
#include <set>

namespace dlbs {

class Topology {
public:
    Topology() {}

    void addNode(int rank);
    void addLink(int source, int dest);

    size_t getNumOfNodes() const;

    std::set<int> getAdjacentNodes(int node) const;

private:
    std::map<int, std::set<int>> links;
};

class TopologyUtil {
public:
    static Topology makeLinearTopology(size_t num_of_nodes);
    static Topology makeCart2DTopology(size_t height, size_t width);
};

}
