#pragma once

#include <vector>
#include <cstddef>

namespace dlbs {

class FragmentMesh {
public:
    FragmentMesh(size_t num_of_dims = 1):
        _dims(num_of_dims, 1) {
    }

    void setDim(size_t index, size_t size) {
        _dims[index] = size;
    }

    size_t dim(size_t index) const {
        return _dims[index];
    }

private:
    std::vector<size_t> _dims;
};

}
