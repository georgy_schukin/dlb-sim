#pragma once

namespace dlbs {

class Fragment {
public:
    void setLoad(const double &load) {
        this->load = load;
    }

    double getLoad() const {
        return load;
    }

private:
    double load {0};
};

}
