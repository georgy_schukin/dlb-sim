#pragma once

#include <vector>

namespace dlbs {

enum QuadrantType {
    NORTH_WEST = 0,
    NORTH_EAST,
    SOUTH_WEST,
    SOUTH_EAST
};

enum DirectionType {
    NORTH = 0,
    SOUTH,
    WEST,
    EAST,
    UNKNOWN_DIR
};

enum DimensionType {
    DIM_X = 0,
    DIM_Y
};

typedef double LoadType;

DirectionType getOppositeDir(DirectionType dir);
std::vector<DirectionType> getAllDirections();

}
