#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "node/node_grid.h"
#include "node/patch_node.h"
#include "grid/load_cell.h"
#include "sim/random_load_simulator.h"
#include "sim/wave_load_simulator.h"
#include "sim/halve_load_simulator.h"

#include <QBrush>
#include <QColor>
#include <QPen>
#include <QGraphicsRectItem>

#include <vector>
#include <cstdlib>
#include <algorithm>
#include <future>
#include <map>
#include <fstream>

namespace {
    const std::vector<QColor>& getColors() {
        static std::vector<QColor> colors = {Qt::red, Qt::green, Qt::blue,
                                             Qt::yellow, Qt::cyan, Qt::magenta,
                                             Qt::darkRed, Qt::darkGreen, Qt::darkBlue,
                                             Qt::darkYellow, Qt::darkCyan, Qt::darkMagenta,
                                             Qt::gray, Qt::darkGray};
        return colors;
    }

    const std::vector<Qt::BrushStyle>& getBrushStyles() {
        static std::vector<Qt::BrushStyle> brush_styles = {Qt::SolidPattern,
                                                           Qt::Dense1Pattern, Qt::Dense2Pattern, Qt::Dense3Pattern,
                                                           Qt::Dense4Pattern, Qt::Dense5Pattern, Qt::Dense6Pattern,
                                                           Qt::CrossPattern,
                                                           Qt::BDiagPattern, Qt::FDiagPattern, Qt::DiagCrossPattern};
        return brush_styles;
    }

    QColor getColorForRank(int rank) {
        return getColors()[rank % getColors().size()];

    }

    Qt::BrushStyle getBrushStyleForRank(int rank) {
        return getBrushStyles()[(rank / getColors().size()) % getBrushStyles().size()];
    }

    int getRectSize() {
        return 16;
    }

    QPoint getRectCenter(const Coord &coord) {
        static const int MARGIN = 4;
        return QPoint(coord.getX()*(getRectSize() + MARGIN),
                      coord.getY()*(getRectSize() + MARGIN));
    }

    QRect getRect(const Coord &coord, int rect_size=getRectSize()) {
        const QPoint center = getRectCenter(coord);
        return QRect(center.x() - rect_size / 2, center.y() - rect_size / 2, rect_size, rect_size);
    }

    template <class T>
    T lerp(T start, T end, double t) {
        return start + (end - start)*t;
    }

    int getRankForCoord(const std::map<int, LoadCellGroup> &cells, const Coord &coord) {
        for (const auto &p: cells) {
            if (p.second.contains(coord)) {
                return p.first;
            }
        }
        return -1;
    }

    const int UPDATE_RATE = 10;

    std::ofstream load_out;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    is_running(false)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);

    update_timer = std::make_shared<QTimer>(this);
    simulation_timer = std::make_shared<QTimer>(this);

    connect(update_timer.get(), &QTimer::timeout, this, &MainWindow::updateScene);
    connect(simulation_timer.get(), &QTimer::timeout, this, &MainWindow::simulationStep);

    load_sim = std::make_shared<RandomLoadSimulator>();

    show_load = ui->showLoad->isChecked();
    simulation_rate = ui->simulationRate->value();
    enable_dlb = ui->enableDLB->isChecked();
    sim_is_active = false;
    simulation_step = 0;

    createGrid();
    startUpdates();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::createGrid() {
    const int nodes_by_x = ui->nodesByX->value();
    const int nodes_by_y = ui->nodesByY->value();
    const int indices_by_x = ui->indicesByX->value();
    const int indices_by_y = ui->indicesByY->value();
    node_grid = std::make_shared<NodeGrid>(nodes_by_x, nodes_by_y, indices_by_x, indices_by_y);
}

void MainWindow::startUpdates() {
    update_timer->start(UPDATE_RATE);
}

void MainWindow::startSimulation() {
    simulation_timer->start(simulation_rate);
    load_out.open("load.txt", std::ios_base::trunc | std::ios_base::out);
    sim_is_active = true;
    simulation_step = 0;
}

void MainWindow::stopSimulation() {
    simulation_timer->stop();
    load_out.close();
    sim_is_active = false;
}

void MainWindow::updateScene() {
    scene.clear();
    const auto max_load = std::max(getMaxLoad(), 1e-5);
    node_grid->forEachNode([this, max_load](PatchNode *node) {
        updateNode(node, max_load);
    });
    showCurrentLoad();
}

void MainWindow::updateNode(const PatchNode *node, LoadType max_load) {
    const QColor node_color = getColorForRank(node->getRank());
    const Qt::BrushStyle node_style = getBrushStyleForRank(node->getRank());
    const QBrush node_brush(node_color, node_style);
    const QPen node_pen(Qt::black);
    for (const LoadCellPtr &cell: node->getCurrentCells()) {
        const Coord coord = cell->getCoord();
        QPoint center = getRectCenter(coord);
        QRect cell_rect = show_load ? getRect(coord, lerp(getRectSize() / 2, getRectSize(), cell->getLoad() / max_load)) : getRect(coord);
        QGraphicsRectItem *cell_item = new QGraphicsRectItem(cell_rect);
        cell_item->setBrush(node_brush);
        cell_item->setPen(node_pen);
        cell_item->setToolTip(QString("Coord (%1, %2), load %3, rank %4").arg(coord.getX()).arg(coord.getY()).arg(cell->getLoad()).arg(node->getRank()));
        scene.addItem(cell_item);
        for (DirectionType dir: getAllDirections()) {
            if (cell->hasAdjacency(dir)) {
                QPoint adj_center = getRectCenter(cell->getAdjacentCellCoord(dir));
                int adj_rank = cell->getAdjacentRank(dir);
                scene.addLine(center.x(), center.y(),
                              center.x() + (adj_center.x() - center.x())/2,
                              center.y() + (adj_center.y() - center.y())/2,
                              QPen(QBrush(getColorForRank(adj_rank)), 2));
            }
        }
    }
}

LoadType MainWindow::getMaxLoad() {
    std::vector<LoadType> max_loads;
    node_grid->forEachNode([&max_loads](PatchNode *node) {
        max_loads.push_back(node->getCurrentCells().getMaxLoad());
    });
    return *std::max_element(max_loads.begin(), max_loads.end());
}

void MainWindow::simulationStep() {
    simulation_step++;
    addLoadToNodes();
    if (enable_dlb) {
        rebalanceNodes();
    }
    outputCurrentLoad();
}

void MainWindow::addLoadToNodes() {
    load_sim->simulate(node_grid.get());
}

void MainWindow::showCurrentLoad() {
    QString load_msg =
            QString("Simulation step: %1\n").arg(simulation_step) +
            QString("Max hops: %1\n").arg(getMaxHops());
    for (int i = 0; i < node_grid->getNumOfNodes(); i++) {
        PatchNode *node = node_grid->getNode(i);
        QString load = QString::number(node->getCurrentLoad(), 'f', 3);
        QString node_str = QString("Rank %1: %2").arg(node->getRank()).arg(load, -10).leftJustified(25);
        load_msg += node_str;
        if (i > 0 && (i + 1) % 4 == 0) {
            load_msg += QString("\n");
        }
    }
    ui->info->setText(load_msg);
}

void MainWindow::outputCurrentLoad() {
    for (int i = 0; i < node_grid->getNumOfNodes(); i++) {
        PatchNode *node = node_grid->getNode(i);
        load_out << node->getCurrentLoad() << "\t";
    }
    load_out << std::endl;
}

int MainWindow::getMaxHops() {
    std::map<int, LoadCellGroup> cells;
    for (int i = 0; i < node_grid->getNumOfNodes(); i++) {
        cells[i] = node_grid->getNode(i)->getCurrentCells();
    }
    int max_hops = 0;
    for (const auto &p: cells) {
        const LoadCellGroup cg = p.second;
        const auto rank = p.first;
        for (const LoadCellPtr &cell: cg) {
            for (DirectionType dir: getAllDirections()) {
                if (cell->hasAdjacency(dir)) {
                    const auto adj_coord = cell->getAdjacentCellCoord(dir);
                    auto adj_rank = getRankForCoord(cells, adj_coord);
                    if (adj_rank != -1) {
                        int hops = node_grid->rankToCoord(rank).getHops(node_grid->rankToCoord(adj_rank));
                        if (hops > max_hops) {
                            max_hops = hops;
                        }
                    }
                }
            }
        }
    }
    return max_hops;
}

void MainWindow::rebalanceNodes() {
    node_grid->forEachNode([](PatchNode *node) {
        std::async(std::launch::async, [node] {
            node->rebalance();
        });
    });
}

void MainWindow::on_startButton_clicked() {
    if (is_running) {
        stopSimulation();
        ui->startButton->setText("Resume");
        is_running = false;
    } else {
        startSimulation();
        ui->startButton->setText("Pause");
        is_running = true;
    }
}

void MainWindow::on_resetButton_clicked() {
    createGrid();
    stopSimulation();
    ui->startButton->setText("Start");
    is_running = false;
}

void MainWindow::on_simType_currentIndexChanged(int index) {
    switch (index) {
    case 0:
        load_sim = std::make_shared<RandomLoadSimulator>();
        break;
    case 1:
        load_sim = std::make_shared<WaveLoadSimulator>();
        break;
    case 2:
        load_sim = std::make_shared<HalveLoadSimulator>(0.5, 0.5);
        break;
    default:
        break;
    }
}

void MainWindow::on_showLoad_toggled(bool checked) {
    show_load = checked;
}

void MainWindow::on_simulationRate_valueChanged(int sim_rate) {
    simulation_rate = sim_rate;
    if (sim_is_active) {
        stopSimulation();
        startSimulation();
    }
}

void MainWindow::on_enableDLB_toggled(bool checked) {
    enable_dlb = checked;
}

void MainWindow::on_actionExit_triggered()
{
    qApp->exit();
}

void MainWindow::on_actionOpen_triggered()
{

}
