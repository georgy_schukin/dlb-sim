QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dlb_vis
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

include(../dlb_lib.pri)
