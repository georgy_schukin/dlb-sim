#pragma once

#include "types.h"

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <memory>

namespace Ui {
    class MainWindow;
}

namespace dlbs {
    class PatchNode;
    class NodeGrid;
    class LoadSimulator;
}

using namespace dlbs;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void createGrid();
    void updateScene();
    void startSimulation();
    void stopSimulation();
    void reset();

private:
    void startUpdates();
    void simulationStep();
    void addLoadToNodes();
    void rebalanceNodes();
    void updateNode(const PatchNode *node, LoadType max_load);

    LoadType getMaxLoad();
    void showCurrentLoad();
    void outputCurrentLoad();

    int getMaxHops();

private slots:
    void on_startButton_clicked();
    void on_resetButton_clicked();
    void on_simType_currentIndexChanged(int index);
    void on_showLoad_toggled(bool checked);

    void on_simulationRate_valueChanged(int sim_rate);

    void on_enableDLB_toggled(bool checked);

    void on_actionExit_triggered();

    void on_actionOpen_triggered();

private:
    Ui::MainWindow *ui;
    QGraphicsScene scene;

private:
    std::shared_ptr<NodeGrid> node_grid;
    std::shared_ptr<LoadSimulator> load_sim;
    std::shared_ptr<QTimer> simulation_timer;
    std::shared_ptr<QTimer> update_timer;
    bool is_running;
    bool show_load;
    bool enable_dlb;
    bool sim_is_active;
    int simulation_rate;
    int simulation_step;
};

