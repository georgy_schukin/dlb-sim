#include "node/node_grid.h"
#include "grid/load_cell.h"

#include <iostream>

using namespace dlbs;

int main(int argc, char *argv[]) {
    NodeGrid grid(2, 2, 8, 8);
    grid.printInfo();    
    return 0;
}
