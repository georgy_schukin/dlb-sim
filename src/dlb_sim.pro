TEMPLATE = subdirs

SUBDIRS = dlb_lib dlb_test \
    dlb_run \
    dlb_vis

dlb_test.depends = dlb_lib
dlb_run.depends = dlb_lib
dlb_vis.depends = dlb_lib
