win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../dlb_lib/release/ -ldlbsim
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../dlb_lib/debug/ -ldlbsim
else:unix: LIBS += -L$$OUT_PWD/../dlb_lib/ -ldlbsim

LIBS += -lpthread

INCLUDEPATH += $$PWD/dlb_lib
DEPENDPATH += $$PWD/dlb_lib